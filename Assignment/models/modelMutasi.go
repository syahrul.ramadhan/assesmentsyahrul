package models

import (
	"assignment/config"
	"fmt"
)

type Transaksi struct {
	Date    int64
	Type    string
	Gram    float64
	Harga   int
	Buyback int
	Saldo   float64
}
type DataTransaksi struct {
	Error bool
	Data  []Transaksi
}

type DapatTransaksi struct {
	Norek      string
	Start_date int64
	End_date   int64
}

func GetMutasi(dataT DapatTransaksi) DataTransaksi{
	var data DataTransaksi
	var dataTR []Transaksi
	db := config.ConnectDB()
	defer db.Close()
	sql := "select date,type,gram,e.harga_topup,e.harga_buyback,saldo from transaksi t join emas e on t.harga_id = e.id where norek = $1 and date between $2 and $3"
	row,err := db.Query(sql,dataT.Norek,dataT.Start_date,dataT.End_date)
	if err != nil {
		fmt.Println("error Query")
	}
	defer row.Close()
	for row.Next() {
		var rows Transaksi
		row.Scan(&rows.Date,&rows.Type, &rows.Gram,&rows.Harga,&rows.Buyback,&rows.Saldo)
		dataTR = append(dataTR, rows)
	}
	data = DataTransaksi{
		Error: false,
		Data: dataTR,
	}
	return data
}