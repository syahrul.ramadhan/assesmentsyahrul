package controller

import (
	"assignment/models"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/teris-io/shortid"
)

func GetEmas(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	marshal, _ := json.MarshalIndent(models.CekHargaEmas(), "", "\t")
	w.Write(marshal)
}

func InsertEmas(w http.ResponseWriter, r *http.Request){
	w.Header().Add("Content-Type", "application/json")
	var insert models.InHarga
	var fNot models.MsgFalse
	var fYes models.Msg
	rand,_ := shortid.Generate()
	err := json.NewDecoder(r.Body).Decode(&insert)
	if err != nil {
		fmt.Print("error models", err)
	}
	result := models.InsertEmas(insert)
	if result == "true" {
		fNot.Status = true
		fNot.Reff_id = rand
		fNot.Message = "Data Salah"
		json.NewEncoder(w).Encode(fNot)
	} else {
		fYes.Status = false
		fYes.Reff_id = rand
		json.NewEncoder(w).Encode(fYes)
	}
}
