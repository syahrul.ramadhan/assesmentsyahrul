package controller

import (
	"assignment/models"
	"encoding/json"
	"fmt"
	"net/http"
)

func BuyBack(w http.ResponseWriter, r *http.Request){
	w.Header().Add("Content-Type", "application/json")
	
	var mbuyback models.BuyBack
	err := json.NewDecoder(r.Body).Decode(&mbuyback)
	if err != nil {
		fmt.Println("post body error :", err)
	}

	errors, idref := "", ""
	idref, errors = models.MBuyback(mbuyback)
	if errors == "" { //respons sukses
		var ress models.Msg
		ress.Status = false
		ress.Reff_id = idref
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		var resf models.MsgFalse
		resf.Status = true
		resf.Reff_id = idref
		resf.Message = "Saldo anda kurang dari buyback saat ini"
		json.NewEncoder(w).Encode(resf)
	}
}