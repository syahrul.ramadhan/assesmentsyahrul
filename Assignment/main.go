package main

import (
	"fmt"
	"log"
	"net/http"
	"assignment/router"
	"github.com/gorilla/handlers"
)

func main() {
	fmt.Println("Server Running at port 80")

	r := router.Router() //memanggil fungsi router
	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"})
	origins := handlers.AllowedOrigins([]string{"*"})
	log.Fatal(http.ListenAndServe(":80", handlers.CORS(headers, methods, origins)(r))) //menjalankan server dengan port 80
}