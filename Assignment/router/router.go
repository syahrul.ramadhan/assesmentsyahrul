package router

import (
	"assignment/controller"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {
	router := mux.NewRouter()
	pre := router.PathPrefix("/api").Subrouter()
	pre.HandleFunc("/check-harga",controller.GetEmas).Methods("GET")
	pre.HandleFunc("/input-harga",controller.InsertEmas).Methods("POST")
	pre.HandleFunc("/topup",controller.Topup).Methods("POST")
	pre.HandleFunc("/saldo",controller.GetSaldo).Methods("POST")
	pre.HandleFunc("/buyback",controller.BuyBack).Methods("POST")
	pre.HandleFunc("/mutasi",controller.GetMutasi).Methods("POST")
	return router
}