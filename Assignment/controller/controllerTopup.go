package controller

import (
	"assignment/models"
	"encoding/json"
	"fmt"
	"net/http"
)

func Topup(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	var mtopup models.Topup
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&mtopup)
	if err != nil {
		fmt.Println("post body error :", err)
	}

	errors, idref := "", ""
	idref, errors = models.MTopup(mtopup)
	if errors == "" { //respons sukses
		var ress models.Msg
		ress.Status = false
		ress.Reff_id = idref
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		var resf models.MsgFalse
		resf.Status = true
		resf.Reff_id = idref
		resf.Message = "Harga dimasukan berbeda dengan harga_topup saat ini"
		json.NewEncoder(w).Encode(resf)
	}
}

func GetSaldo(w http.ResponseWriter, r *http.Request){
	var saldo models.CekSaldo
	err := json.NewDecoder(r.Body).Decode(&saldo)
	if err != nil {
		fmt.Println("error decode :",err)
	}
	marshal,_ := json.MarshalIndent(models.GetRek(saldo),"","\t")
	w.Write(marshal)
}