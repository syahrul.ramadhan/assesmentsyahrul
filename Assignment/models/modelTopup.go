package models

import (
	"assignment/config"
	"fmt"
	"math"
	"strconv"
	"time"

	"github.com/teris-io/shortid"
)

type Topup struct {
	Gram  string
	Harga string
	Norek string
}

type CekSaldo struct{
	Norek string `json:"norek"`
}

type Saldo struct{
	Error bool
	Data GetSaldo
}

type GetSaldo struct{
	Norek string
	Saldo float32
}

func GetRek (norek CekSaldo)Saldo{
	var saldo Saldo
	db:=config.ConnectDB()
	defer db.Close()
	sql := "select norek,saldo from saldo where norek = $1"
	row,err := db.Query(sql,norek.Norek)
	if err != nil {
		fmt.Println("error Query:",err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&saldo.Data.Norek,&saldo.Data.Saldo)
		saldo.Error = false 
	}
	return saldo
}


func TimeStamp() int64{
	return time.Now().UnixNano()/int64(time.Millisecond)
}

func MTopup(mtopup Topup) (string, string) {
	refid, _ := shortid.Generate()
	sError := ""

	//ambil harga topup saat ini
	htopup := getHargaTopUp()
	dataGram,_ := strconv.ParseFloat(mtopup.Gram,64)
	dataGram = roundFloat(dataGram,3)
	//cek harga topup
	iTopup, _ := strconv.Atoi(mtopup.Harga) //string to int
	if iTopup == htopup && dataGram >= 0.001 {
		//insert tbl_topup
		insertTranTopup(mtopup)
		//hitung saldo norek
		tSaldo := getSaldoRek(mtopup.Norek)
		tSaldo = roundFloat(tSaldo,3)
		//update tbl_rek norek
		updateRekening(mtopup.Norek, tSaldo)
		t := TimeStamp()
		ReTransaksi(mtopup.Norek,t,"topup",dataGram,tSaldo)
	} else {
		sError = "1"
	}
	return refid, sError
}

func getHargaTopUp() int {
	db := config.ConnectDB()
	defer db.Close()
	//sql
	sql := "select harga_topup from emas"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var htopup int
	for rows.Next() {
		err = rows.Scan(&htopup) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}
	return htopup
}

func insertTranTopup(mtopup Topup) {
	db := config.ConnectDB()
	defer db.Close()
	data,_ := strconv.ParseFloat(mtopup.Gram,64)
	data = roundFloat(data,3)
	sql := "insert into topup(norek,gram,harga)VALUES($1,$2,$3)"
	_, err := db.Exec(sql, mtopup.Norek, data, mtopup.Harga)
	if err != nil {
		fmt.Println("sql insert topup : ", err)
	}
}

func getSaldoRek(rek string) float64 {
	db := config.ConnectDB()
	defer db.Close()
	sql := "select sum(gram) from topup where norek=$1 GROUP BY norek"
	rows, err := db.Query(sql, rek)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var saldo float64
	for rows.Next() {
		err = rows.Scan(&saldo)
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}
	return saldo
}

func updateRekening(norek string, saldo float64) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "update saldo set saldo=$1 where norek=$2"
	_, err := db.Exec(sql, saldo, norek)
	if err != nil {
		fmt.Println("sql update rek : ", err)
	}
}

func roundFloat(val float64, precision uint) float64 {
    ratio := math.Pow(10, float64(precision))
    return math.Round(val*ratio) / ratio
}