package models

import (
	"assignment/config"
	"fmt"

	"github.com/teris-io/shortid"
)

type BuyBack struct {
	Gram  float64
	Harga int
	Norek string
}

func MBuyback(buy BuyBack) (string, string) {
	refid, _ := shortid.Generate()
	sError := ""

	//ambil harga topup saat ini
	htopup := getSaldoBuyback(buy.Norek)
	harga := buy.Gram
	//cek harga topup
	if harga <= htopup {
		//insert tbl_topup
		insertTranBuyback(buy)
		//hitung saldo norek
		tSaldo := getSaldoRek(buy.Norek)
		minSaldo := tSaldo - harga
		//update tbl_rek norek
		updateRekening(buy.Norek, minSaldo)
		t := TimeStamp()
		ReTransaksi(buy.Norek,t,"buyback",harga,minSaldo)
	} else {
		sError = "1"
	}
	return refid, sError
}

func getSaldoBuyback(ceks string)float64{
	db := config.ConnectDB()
	defer db.Close()
	sql := "select saldo from saldo where norek=$1"
	rows, err := db.Query(sql, ceks)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var saldo float64
	for rows.Next() {
		err = rows.Scan(&saldo)
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}
	return saldo
}
func insertTranBuyback(mbuyback BuyBack) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "insert into buyback(norek,gram,harga)VALUES($1,$2,$3)"
	_, err := db.Exec(sql, mbuyback.Norek, mbuyback.Gram, mbuyback.Harga)
	if err != nil {
		fmt.Println("sql insert topup : ", err)
	}
}

func ReTransaksi(no string, date int64, tipe string, gr float64, saldo float64){
	db := config.ConnectDB()
	defer db.Close()

	sql := "insert into transaksi (norek,date,type,gram,saldo,harga_id) values($1,$2,$3,$4,$5,1)"
	_,err := db.Exec(sql,no,date,tipe,gr,saldo)
	if	err != nil {
		fmt.Println("error Exec :", err)
	}
}