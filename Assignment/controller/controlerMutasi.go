package controller

import (
	"assignment/models"
	"encoding/json"
	"fmt"
	"net/http"
)

func GetMutasi(w http.ResponseWriter, r *http.Request){
	w.Header().Add("Content-Type", "application/json")
	var data models.DapatTransaksi
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		fmt.Println("gagal json mutasi :", err)
	}
	marshal, _ := json.MarshalIndent(models.GetMutasi(data), "", "\t")
	w.Write(marshal)
}