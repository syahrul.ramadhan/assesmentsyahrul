package models

import (
	"assignment/config"
	"fmt"
)

type Emas struct{
	Error bool
	Data Harga
}

type Msg struct{
	Status bool
	Reff_id string
}
type MsgFalse struct{
	Status bool
	Reff_id string
	Message string
}

type InHarga struct{
	Admin_id string
	Harga_buyback string
	Harga_topup string
}

type Harga struct{
	Harga_buyback int 
	Harga_topup int
}

func CekHargaEmas()Emas {
	var emas Emas
	db := config.ConnectDB()
	defer db.Close()
	sql := "select harga_buyback,harga_topup from emas"
	row ,err := db.Query(sql)
	if	err != nil {
		fmt.Println("error db :", err)
	}
	defer row.Close()
	for	row.Next(){
		row.Scan(&emas.Data.Harga_buyback,&emas.Data.Harga_topup)
	}
	return emas
}

func InsertEmas(data InHarga)string{
	var status string
	
	db := config.ConnectDB()
	defer db.Close()
	if data.Admin_id == "a001" {
		sql := "update emas set harga_buyback=$1, harga_topup=$2 where admin_id=$3"
		_,err := db.Exec(sql,data.Harga_buyback,data.Harga_topup,data.Admin_id)
		if err != nil {
			fmt.Println("Error db :", err)
		
		}
		status = "false"
	}else {
		status = "true"
	}
	return status
}